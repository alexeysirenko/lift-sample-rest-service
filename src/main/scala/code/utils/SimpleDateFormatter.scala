package code.utils


import java.text.SimpleDateFormat
import java.util.TimeZone

/**
 * Custom date formatter.
 */
object SimpleDateFormatter {

  /**
   * Creates new formatter instance.
   *
   * Since formatter is not thread-safe it should be created for every method call
   * @return formatter instance
   */
  def dateFormat: SimpleDateFormat = {
    val format = new SimpleDateFormat("yyyy-MM-dd")
    format.setTimeZone(TimeZone.getTimeZone("GMT"))
    format
  }
}

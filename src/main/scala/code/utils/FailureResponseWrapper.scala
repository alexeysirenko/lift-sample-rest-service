package code.utils

import net.liftweb.json._

/**
 * Wrapper for meaningful information of a failure.
 *
 * @param status status code
 * @param errMsg error message
 */
case class FailureResponseWrapper(status: String, errMsg: String)

object FailureResponseWrapper {

  /**
   * Creates a wrapper of a throwable instance.
   *
   * @param e throwable object
   * @return wrapper instance
   */
  def apply(e: Throwable): FailureResponseWrapper = {
    FailureResponseWrapper(e.getMessage)
  }

  /**
   * Creates a wrapper of an error message.
   *
   * @param errMsg error message
   * @return wrapper instance
   */
  def apply(errMsg: String): FailureResponseWrapper = {
    FailureResponseWrapper("error", errMsg)
  }

  /**
   * JSON serializer.
   */
  implicit def toJson(item: FailureResponseWrapper)(implicit formats: Formats = net.liftweb.json.DefaultFormats): JValue = {
    Extraction.decompose(item)
  }

}

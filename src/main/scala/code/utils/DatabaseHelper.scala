package code.utils

import net.liftweb.common.Logger
import net.liftweb.util.Props
import org.squeryl.adapters.{H2Adapter, MySQLAdapter}
import org.squeryl.{Session, SessionFactory}

/**
 * Database helper object to initialize database session.
 */
object DatabaseHelper extends Logger {

  def initializeSessionFactory(driver: String, database: String, user: String, password: String): Unit = {
    info(s"Try to initialize database session factory with parameters [$driver : $database] for user $user/${password.replaceAll(".", "*")}")

    val adapter = driver match {
      case "com.mysql.jdbc.Driver" =>
        info(s"Creating MySQL database adapter...")
        new MySQLAdapter
      case _ =>
        info(s"Creating default H2 database adapter...")
        new H2Adapter
    }

    Class.forName(driver)
    SessionFactory.concreteFactory = Some(() => Session.create(
      java.sql.DriverManager.getConnection(database, user, password), adapter)
    )
    info(s"Database session initialized successfully")
  }

  def initializeSessionFactory(): Unit = {
    info(s"Initialize database session")
    (for {
      driver <- Props.get("db.driver")
      database <- Props.get("db.url")
      user <- Props.get("db.user")
      password <- Props.get("db.password")
    } yield {
        initializeSessionFactory(driver, database, user, password)
      }) getOrElse {
      throw new IllegalArgumentException("Unable to establish database connection with missing configuration parameters")
    }
  }
}

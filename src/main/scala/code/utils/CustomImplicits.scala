package code.utils

import java.sql.Timestamp
import java.util.Date

/**
 * Custom conversion methods.
 */
object CustomImplicits {

  /**
   * Converts date into timestamp.
   *
   * @param date date
   * @return timestamp
   */
  implicit def date2timestamp(date: Date): Timestamp = new Timestamp(date.getTime)

}

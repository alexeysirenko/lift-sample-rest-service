package code
package model

import java.sql.Timestamp

import code.utils.SimpleDateFormatter._
import code.utils.{CustomImplicits, SimpleDateFormatter}
import net.liftweb.json._
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.{KeyedEntity, Schema}

/**
 * Customer model.
 *
 * @param id unique id
 * @param firstName first name
 * @param lastName last name
 * @param birthday date of birth
 */
case class Customer(id: Option[Long],
                    firstName: String,
                    lastName: String,
                    birthday: Option[Timestamp]) extends KeyedEntity[Option[Long]]

object Customer extends Schema {

  private val customers = table[Customer]("customers")

  import CustomImplicits._

  implicit val liftJsonFormats = new DefaultFormats {
    override def dateFormatter = SimpleDateFormatter.dateFormat
  }

  def apply(firstName: String, lastName: String, birthday: Option[Timestamp]): Customer = {
    Customer(None, firstName, lastName, birthday)
  }

  def add(customer: Customer): Customer = {
    inTransaction {
      customers.insert(customer)
    }
  }

  def update(customer: Customer): Unit = {
    inTransaction {
      customers.update(customer)
    }
  }

  def get(id: Long): Option[Customer] = {
    inTransaction {
      customers.where(a => a.id === id).headOption
    }
  }

  def get(whereParams: Map[String, String]): Seq[Customer] = {
    inTransaction {
      from(customers)(c =>
        where(
          (c.id === whereParams.get("id").map(_.toLong).?) and
          (c.firstName === whereParams.get("firstName").?) and
          (c.lastName === whereParams.get("lastName").?) and
          (c.birthday === (whereParams.get("birthday").map(birthday => dateFormat.parse(birthday)): Option[Timestamp]).?)
        )
        select c
      ).toList
    }
  }

  def delete(id: Long): Int = {
    inTransaction {
      customers.deleteWhere(a => a.id === id)
    }
  }


}


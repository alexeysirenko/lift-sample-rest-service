package code.lib

import java.sql.Timestamp
import java.util.Date

import code.model.Customer
import code.utils.SimpleDateFormatter._
import code.utils.{CustomImplicits, FailureResponseWrapper}
import net.liftweb.common.Logger
import net.liftweb.http.rest.RestHelper
import net.liftweb.http.{JsonResponse, S}
import net.liftweb.json.Extraction._

import scala.util.{Failure, Success, Try}

/**
 * Customer API controller.
 */
object CustomerRest extends RestHelper with Logger {

  override implicit val formats = Customer.liftJsonFormats

  import CustomImplicits._

  serve("api" / "customer" prefix {

    /**
     * Get customer by id
     */
    case itemId :: Nil JsonGet _ if itemId matches """\d+""" =>
      Try({
        info(s"Get customer with id $itemId")
        Customer.get(itemId.toLong)
      }) match {
        case Success(Some(customer)) => JsonResponse(decompose(Map("status" -> "OK", "data" -> customer)))
        case Success(_) => JsonResponse(FailureResponseWrapper(s"Customer with id $itemId not found"), 204)
        case Failure(ex) =>
          info(s"Internal server error", ex)
          JsonResponse(FailureResponseWrapper(ex), 500)
      }

    /**
     * Get list of customers matching filter parameters
     */
    case Nil Get _  =>
      Try({
        val whereParameters = S.request.toOption match {
          case Some(request) if request.params.nonEmpty => request.params.map { case (k, v) => (k, v.head) }
          case _ => throw new IllegalArgumentException("You have to specify filter parameters")
        }
        info(s"Get customer with parameters $whereParameters")
        Customer.get(whereParameters)
      }) match {
        case Success(customers) => JsonResponse(decompose(Map("status" -> "OK", "data" -> customers)))
        case Failure(ex: IllegalArgumentException) => JsonResponse(FailureResponseWrapper(ex), 204)
        case Failure(ex) =>
          info(s"Internal server error", ex)
          JsonResponse(FailureResponseWrapper(ex), 500)
      }


    /**
     * Add a new customer
     */
    case Nil JsonPost json->body =>
      Try({
        val jValue = body.json getOrElse (throw new IllegalArgumentException("Missing JSON data input"))
        val firstName = (jValue \ "firstName").extract[String]
        val lastName = (jValue \ "lastName").extract[String]
        val birthday = (jValue \ "birthday").toOpt.map(value => dateFormat.parse(value.extract[String])): Option[Timestamp]
        val newCustomer = Customer(firstName, lastName, birthday)
        info(s"Add new customer with parameters $newCustomer")
        Customer.add(newCustomer)
      }) match {
        case Success(customer) => JsonResponse(decompose(Map("status" -> "OK", "data" -> customer)))
        case Failure(ex: IllegalArgumentException) => JsonResponse(FailureResponseWrapper(ex), 400)
        case Failure(ex) =>
          info(s"Internal server error", ex)
          JsonResponse(FailureResponseWrapper(ex), 500)
      }


    /**
     * Update existing customer
     */
    case itemId :: Nil JsonPut json->body if itemId matches """\d+""" =>
      Try({
        val jValue = body.json getOrElse (throw new IllegalArgumentException("Missing JSON data input"))
        val firstName = (jValue \ "firstName").extract[String]
        val lastName = (jValue \ "lastName").extract[String]
        val birthday = (jValue \ "birthday").toOpt.map(value => dateFormat.parse(value.extract[String])): Option[Timestamp]
        val updateCustomer = Customer(Some(itemId.toLong), firstName, lastName, birthday)
        info(s"Update customer $itemId with parameters $updateCustomer")
        Customer.update(updateCustomer)
      }) match {
        case Success(_) => JsonResponse(decompose(Map("status" -> "OK")))
        case Failure(ex: IllegalArgumentException) => JsonResponse(FailureResponseWrapper(ex), 400)
        case Failure(ex) =>
          info(s"Internal server error", ex)
          JsonResponse(FailureResponseWrapper(ex), 500)
      }


    /**
     * Delete customer by its id
     */
    case itemId :: Nil JsonDelete _ if itemId matches """\d+""" =>
      Try({
        info(s"Delete customer with id $itemId")
        Customer.delete(itemId.toLong)
      }) match {
        case Success(nRemoved) if nRemoved > 0 => JsonResponse(decompose(Map("status" -> "OK")))
        case Success(_) => JsonResponse(FailureResponseWrapper(s"Customer with id $itemId not found"), 400)
        case Failure(ex) =>
          info(s"Internal server error", ex)
          JsonResponse(FailureResponseWrapper(ex), 500)
      }

    /**
     * Handle invalid API requests
     */
    case _ => JsonResponse(FailureResponseWrapper("Unknown API request"), 400)
  })

}

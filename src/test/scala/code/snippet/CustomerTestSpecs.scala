package code
package snippet

import java.sql.Timestamp
import java.util.Calendar

import code.model.Customer
import code.utils.DatabaseHelper
import net.liftweb.util._
import org.specs2.mutable.Specification
import org.specs2.specification._
import org.squeryl.PrimitiveTypeMode._


object CustomerTestSpecs  extends Specification with BeforeEach {

  sequential

  override def before = initialize()

  "Customer DAO should" >> {

    "allow to create a new customer" >> {
      val newCustomer = makeUniqueCustomer
      val addedCustomer = Customer.add(newCustomer)
      (newCustomer.lastName == addedCustomer.lastName &&
      newCustomer.firstName == addedCustomer.firstName &&
      newCustomer.birthday.get.equals(addedCustomer.birthday.get)) must_== true
    }

    "allow to select a customer by id" >> {
      val newCustomer = makeUniqueCustomer
      val addedCustomer = Customer.add(newCustomer)
      val selectedCustomer = Customer.get(addedCustomer.id.get).get
      (newCustomer.lastName == selectedCustomer.lastName &&
        newCustomer.firstName == selectedCustomer.firstName &&
        newCustomer.birthday.get.equals(selectedCustomer.birthday.get)) must_== true
    }

    "allow to select customers by specific parameters" >> {
      val COUNT = 3
      val firstName = StringHelpers.randomString(10)
      val newCustomers = (1 to COUNT) map (n => makeUniqueCustomer.copy(firstName = firstName))
      newCustomers foreach (newCustomer => Customer.add(newCustomer))
      val query = Map("firstName" -> firstName)
      val selectedCustomers = Customer.get(query)
      selectedCustomers.size must_== COUNT
    }

    "allow to update an existing customer" >> {
      val newCustomer = Customer.add(makeUniqueCustomer)
      val randomFirstName = StringHelpers.randomString(10)
      val randomLastName = StringHelpers.randomString(10)
      val updatedCustomer = newCustomer.copy(firstName = randomFirstName, lastName = randomLastName)
      Customer.update(updatedCustomer)
      val selectedCustomer = Customer.get(updatedCustomer.id.get).get
      (selectedCustomer.firstName == randomFirstName &&
        selectedCustomer.lastName == randomLastName &&
        selectedCustomer.birthday.get.equals(updatedCustomer.birthday.get)) must_== true
    }

    "allow to delete a customer by id" >> {
      val newCustomer = Customer.add(makeUniqueCustomer)
      Customer.delete(newCustomer.id.get) must_== 1
      Customer.get(newCustomer.id.get) must_== None
    }
  }

  private def makeUniqueCustomer = {
    val now = Calendar.getInstance.getTime
    val randomFirstName = StringHelpers.randomString(10)
    val randomLastName = StringHelpers.randomString(10)
    Customer(randomFirstName, randomLastName, Some(new Timestamp(now.getTime)))
  }

  private def initialize() = {
    DatabaseHelper.initializeSessionFactory("org.h2.Driver", "jdbc:h2:~/tests", "root", "")
    inTransaction {
      Customer.drop
      Customer.create
    }
  }
}
